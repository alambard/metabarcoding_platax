# Study and data 

Monitoring of two field episode of tenacibaculosis infection of Platax fish using metabarcoding or qPCR targetting Tenacibaculum maritimum bacterial species

V4 16S metabarcoding analysis on platax skin smears (mainly) but also on seawater column, breeding structures smears (cages). Some DNA fish sample were also analysed on V1V2 region to try to detect more precisely  species of Tenacibaculum genus

# R packages

- metabarcoding workflow : dada2
- Data manipulation : ShortRead, stringr, Biostrings, dplyr
- Biological analyses : phyloseq, Rmisc, vegan, microbiome
- Phylogenetic tree building : DECIPHER, phangorn, ape

# Online ressources 

https://benjjneb.github.io/dada2/tutorial_1_8.html

https://joey711.github.io/phyloseq/

https://f1000research.com/articles/5-1492/v2






