library(phyloseq); packageVersion("phyloseq")
library(Biostrings); packageVersion("Biostrings")
library(ggplot2); packageVersion("ggplot2")
library(dplyr)
library(Rmisc)
library(vegan)
library(ape)
theme_set(theme_bw())

# load dada2 output files for phyloseq
load("taxa.Rdata")
load("seqtab.nochim.Rdata")
load(".RData")

# prepare files
samples.out <- rownames(seqtab.nochim)
sample.data<-read.table("data/metadata_platax.txt",header = T)
rownames(sample.data)<-sample.data$Name
sample.data$Group<-paste(sample.data$Site,"_",sample.data$Type)
head(sample.data)
Time<-sample.data$Time
Station<-sample.data$Site
Type<-sample.data$Type
Group<-sample.data$Group

# create phyloseq object from dada2 outputs
ps <- phyloseq(otu_table(seqtab.nochim, taxa_are_rows=FALSE), sample_data(sample.data), tax_table(taxa), treephy)

            
# Remove samples that are problematic or not included in the analysis

ps = subset_samples(ps, sample_names(ps) != "J0-31_Pas_A_12") #Vaia
ps = subset_samples(ps, sample_names(ps) != "J0-31_Pas_A_10") #Vaia
ps = subset_samples(ps, sample_names(ps) != "J0-31_Pas_A_9") #Vaia
ps = subset_samples(ps, sample_names(ps) != "J0-31_Pas_A_13") #Vaia
ps = subset_samples(ps, sample_names(ps) != "J0-31_Pas_A_11") #vaia
ps = subset_samples(ps, sample_names(ps) != "J14_Pas_F_38") #Vaia
ps = subset_samples(ps, sample_names(ps) != "J14_Pas_F_39") #Vaia
ps = subset_samples(ps, sample_names(ps) != "J14_Pas_F_37") #Vaia
ps = subset_samples(ps, sample_names(ps) != "J14_Pas_F_40") #Vaia

ps <- prune_samples(sample_names(ps), ps) # Remove mock sample

# store the DNA sequences of our ASVs in the refseq slot of the phyloseq object , using short names for ASVs rather than full DNA sequence name
dna <- Biostrings::DNAStringSet(taxa_names(ps))
names(dna) <- taxa_names(ps)
ps <- merge_phyloseq(ps, dna)
taxa_names(ps) <- paste0("ASV", seq(ntaxa(ps)))
ps

## Keep only "Bacteria" domain
erie <- ps %>%
  subset_taxa(
    Kingdom == "Bacteria" &
      Family  != "mitochondria" &
      Class   != "Chloroplast"
  )

erie

### Coverage distribution
# Make a data frame with a column for the read counts of each sample
sample_sum_df <- data.frame(sum = sample_sums(erie))
TEMP=summarySE(sample_sum_df, measurevar="sum")

# creating a matrix object from phyloseq object in order to use vegan package for rarecurve
mat <- (otu_table(erie))
class(mat) <- "matrix"
mat <- as(t(otu_table(erie)), "matrix")
class(mat)

#plot rarefaction curves
raremax <- min(rowSums(mat))
rarecurve(mat, step=50, cex=0.5,col = "blue",sample = raremax,label=FALSE)

# Histogram of sample read counts
ggplot(sample_sum_df, aes(x = sum)) + 
  geom_histogram(color = "black", fill = "indianred", binwidth = 2500) +
  ggtitle("Distribution of sample sequencing depth") + 
  xlab("Read counts") +
  theme(axis.title.y = element_blank())

# mean, max and min of sample read counts
smin <- min(sample_sums(erie))
smean <- mean(sample_sums(erie))
smax <- max(sample_sums(erie))
smin
smean
sample_sums(erie)
write.table(sample_sums(erie),file="sample.sums.txt",quote=F)

############# alpha divertity analysis ##############
rich<-estimate_richness(erie, split = TRUE, measures = c("Chao1","Shannon","Simpson"))
rich

# renaiming
sample.data <- data.frame(lapply(sample.data, function(x) {
  gsub("-", ".", x)
}))
rownames(sample.data)<-sample.data$Name

# compute alpha diversity
rich.merged<-merge(rich,sample.data)
rich.merged
rich.merged %>%
  group_by(Site,Type)%>%
  summarise_at(vars(Shannon), funs(mean(., na.rm=TRUE)))

rich.merged %>%
  group_by(Site,Type) %>%
  summarise_at(vars(Shannon), funs(sd(., na.rm=TRUE)/sqrt(length(.))))

### Plot richness totale
plot_richness(erie, x="Group",color="Group",measures=c("Observed","Chao1","Shannon","Simpson"))


## Plot alpha -richness with shannon

# change riche.merged object for visualization
rich.plot <- rich.merged[sample(nrow(rich.merged), 100), ]
rich.plot <- rich.plot %>% filter(!grepl("VAIA", Group))


richal<-ggplot(rich.plot, aes(y=Shannon, x=Group))+
  labs(x="",y="Shannon indices") +
  geom_boxplot()+
  geom_dotplot(binaxis='y', stackdir='center',bin=10, aes(fill=Group)) +
  ggtitle(label="") +
  scale_x_discrete(labels=c("Tahaa_sain","Tahaa_sympto","Tahiti_sain","Tahiti_sympto"))

richal

### Test richness alpha
pairwise.wilcox.test(rich.merged$Shannon, sample_data(rich.merged)$Group)


## Represent the relative abundance of the most abundant phyla
# prune out phyla below 2% in each sample
erie_phylum <- erie %>%
  tax_glom(taxrank = "Phylum") %>%                     # agglomerate at phylum level
  transform_sample_counts(function(x) {x/sum(x)} ) %>% # Transform to rel. abundance
  psmelt() %>%                                         # Melt to long format
  filter(Abundance > 0.002) %>%                         # Filter out low abundance taxa
  arrange(Phylum)                                      # Sort data frame alphabetically by phylum


# Plot 
ggplot(erie_phylum, aes(x = Group, y = Abundance, fill = Phylum)) + 
  geom_bar(stat = "identity") +
  # Remove x axis title
  theme(axis.title.x = element_blank()) + 
  #
  guides(fill = guide_legend(reverse = TRUE, keywidth = 1, keyheight = 1)) +
  ylab("Relative Abundance (Phyla > 2%) \n") +
  ggtitle("Phylum Composition by group")

###### Beta diversity analysis ##########

## function scale_reads()
scale_reads <- function(physeq, n) {
  physeq.scale <-
    transform_sample_counts(physeq, function(x) {
      (n * x/sum(x))
    })
  otu_table(physeq.scale) <- floor(otu_table(physeq.scale))
  physeq.scale <- prune_taxa(taxa_sums(physeq.scale) > 0, physeq.scale)
  return(physeq.scale)
}

#normalise the phyloseq object to the minimum number of observations (coverage)  , 
#in order to perform beta diversity (sample comparison)
erie_scale <- scale_reads(erie,min(sample_sums(erie)))

# Ordinate
erie_pcoa <- ordinate(
  physeq = erie_scale, 
  method = "PCoA", 
  distance = "bray"
)

#### Ordination NMDS
set.seed(1)

# Transform data to proportions as appropriate for Bray-Curtis distances
ps.prop <- transform_sample_counts(erie_scale, function(otu) otu/sum(otu))
ord.pcoa.bray <- ordinate(ps.prop, method="PCoA", distance="bray")

# Plot PCoA

plot_PCOA_all0.02<-plot_ordination(
  ps.prop, ord.pcoa.bray,
  title = "PCoA Normalization") + 
  geom_point(aes(fill = Type,shape=Site), label=ps.prop@sam_data$Name,size=4,color="black",stroke = 0.5,alpha=0.7) +
  geom_hline(yintercept=0, linetype="dotted") +
  geom_vline(xintercept=0, linetype="dotted")+
  
  scale_fill_manual(values=c("dodgerblue","Tomato2"),
                    name= "Groups",labels=c("Sain","Infectes")) +
  scale_shape_manual(values=c(21,22), labels=c("Tahaa","Tahiti"))

plot_PCOA_all0.02   # similar taxa composition between infected samples , merging in one main cluster

## Test beta-diversity
wunifrac_dist = phyloseq::distance(erie_scale, method="unifrac", weighted=F)
adonis(wunifrac_dist ~ sample_data(erie_scale)$Group,permutations=1000) #parametric
anosim(wunifrac_dist,sample_data(erie_scale)$Group, permutations = 1000) #non-parametric

# Calculate bray curtis distance matrix
erie_bray <- phyloseq::distance(ps.prop, method = "bray") #erie_Scale
disp.age = betadisper(erie_bray, sample_data(erie_scale)$Group)
anova(disp.age)

#Perform an ANOVA-like test to determine if the variances differ by groups.
pmod<-permutest(disp.age, pairwise=TRUE, permutations=1000)
pmod

#### abundance representation
top20 <- names(sort(taxa_sums(ps), decreasing=TRUE))[1:30]
ps.top20 <- transform_sample_counts(ps, function(OTU) OTU/sum(OTU))
ps.top20 <- prune_taxa(top20, ps.top20)
plot_bar(ps.top20, x="Group", fill="Family")

#plot network
plot_net(ps, distance = "bray",maxdist = 0.7,laymeth = "circle")

### Focus on Tenacibaculum
gpac <- subset_taxa(ps, Genus=="Tenacibaculum")
top20 <- names(sort(taxa_sums(gpac), decreasing=TRUE))[1:4]
ps.top20 <- transform_sample_counts(ps, function(OTU) OTU/sum(OTU))
ps.top20 <- prune_taxa(top20, ps.top20)
plot_bar(ps.top20, x="Group", fill="Species")

## "core microbiome" seeking
library(microbiome)
library(RColorBrewer)
library(ggpubr)

# convert to relative abundance  
ps1.rel <- microbiome::transform(ps, "compositional")
print(ps1.rel)

ps1.rel2 <- prune_taxa(taxa_sums(ps1.rel) > 0, ps1.rel)

print(ps1.rel2)
core.taxa.standard <- core_members(ps1.rel2, detection = 0.001, prevalence = 50/100)
taxonomy <- as.data.frame(tax_table(ps1.rel2))
core_taxa_id <- subset(taxonomy, rownames(taxonomy) %in% core.taxa.standard)

DT::datatable(core_taxa_id)
core.abundance <- sample_sums(core(ps1.rel2, detection = 0.001, prevalence = 50/100))
DT::datatable(as.data.frame(core.abundance))

# Core with compositionals:
prevalences <- seq(.05, 1, .05)
detections <- 10^seq(log10(1e-3), log10(.2), length = 10)

library(viridis)
p.core <- plot_core(ps1.rel2, 
                    plot.type = "heatmap", 
                    colours = rev(brewer.pal(5, "Spectral")),
                    prevalences = prevalences, 
                    detections = detections, 
                    min.prevalence = .5) + 
  xlab("Detection Threshold (Relative Abundance (%))")

print(p.core) 
